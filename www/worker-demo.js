/*
    A simple demo for using a sync file access in Web Worker
*/

console.log('we are in the worker demo!');

onmessage = async function(e) {
    console.log('received a message:', e.data);
};
