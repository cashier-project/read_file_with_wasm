console.log('inside worker.');

onmessage = async (e) => {
  let msg = e.data;
  console.log('received:', msg);

  // obtain the sync handle
  let fileHandle = msg.handle;
  // only inside web workers!
  const accessHandle = await fileHandle.createSyncAccessHandle();
  console.log('access handle:', accessHandle);

  const fileSize = accessHandle.getSize();
  console.log('file size:', fileSize);

  // add content to the file if empty.
  if (fileSize == 0) {
    let arrayBuffer = getArrayBufferFromText('test');
    accessHandle.write(arrayBuffer);
    accessHandle.flush();
    accessHandle.close();
  }

  // this can be passed to Wasm
  
};

function getArrayBufferFromText(text) {
  const encoder = new TextEncoder();
  const encodedText = encoder.encode(text);
  const arrayBuffer = encodedText.buffer;

  //let arrayBuffer = new ArrayBuffer(.length);
  return arrayBuffer;
}
