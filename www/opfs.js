/*
    Read a file from OPFS with WASM
*/

/**
 * List directory contents
 * @param {FileSystemDirectoryEntry} dir 
 * @returns An array of items in the given directory.
 */
async function list_entries(dir) {
    let names = [];

    let entries = dir.entries();
    // console.log('entries:', entries);

    for await (const e of entries) {
        names.push(e);
    }

    return names;
}

async function use_opfs() {
    console.log('Working with OPFS:');

    // Create a file in OPFS.
    let root = await navigator.storage.getDirectory();
    console.log('root=', root);

    let entries = await list_entries(root);
    console.log('OPFS entries: ', entries);

    let fileHandle = await root.getFileHandle("file.txt", { create: true });
    console.log('fileHandle:', fileHandle);

    // prevent caching the worker file
    var worker = new Worker('./worker-opfs.js' + '?' + Math.random());
    worker.postMessage({ handle: fileHandle });

    let file = await fileHandle.getFile();
    console.log('file:', file);

    let reader = new FileReader();
    console.log('reader:', reader);
}

use_opfs();
